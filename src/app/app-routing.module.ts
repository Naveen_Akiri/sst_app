import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminOneComponent } from './admin-one/admin-one.component';
import { AdminTwoComponent } from './admin-two/admin-two.component';
import { AppComponent } from './app.component';
import { DataBookComponent } from './data-book/data-book.component';
import { LoggedVehDetailsComponent } from './data-book/logged-veh-details/logged-veh-details.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = 
[
  {path:'',component:HomeComponent},
  {path:'dataBook' , component:DataBookComponent},
  {path:'loggedvehicles' , component:LoggedVehDetailsComponent},
  {path:'admin_one' , component:AdminOneComponent},
  {path:'admin_two' , component:AdminTwoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule 
{

 }
