import { TestBed } from '@angular/core/testing';

import { LoggedVehiclesService } from './logged-vehicles.service';

describe('LoggedVehiclesService', () => {
  let service: LoggedVehiclesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoggedVehiclesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
