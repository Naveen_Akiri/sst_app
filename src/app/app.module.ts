import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DataBookComponent } from './data-book/data-book.component';
import { LoggedVehDetailsComponent} from './data-book/logged-veh-details/logged-veh-details.component';
import { HomeComponent } from './home/home.component';
import { LoggedVehiclesService } from './logged-vehicles.service';
import { AdminOneComponent } from './admin-one/admin-one.component';
import { AdminTwoComponent } from './admin-two/admin-two.component'


@NgModule({
  declarations: [
    AppComponent,
    DataBookComponent,
    HomeComponent,
    LoggedVehDetailsComponent,
    AdminOneComponent,
    AdminTwoComponent
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [LoggedVehiclesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
