import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggedVehiclesService {
  constructor() { }
  logVehicle=
  [
    {
      // S_no:1,
      Date:'2023-06-23',
      VehicleNumber:'AP 31 TH 7677',
      Quantity:26,
      LoadingPoint:'VCT',
      UnloadingPoint:'JK Rayagada'
    }
  ]

  loggVeh(
    // sno:number,
    date:string,vehicleNo:string,Quantity:number,loadingpoint:string,UnLoading_point:string)
  {
   this.logVehicle.push(
    {
      // S_no:sno,
      Date:date.toString(),
      VehicleNumber:vehicleNo,
      Quantity:Quantity,
      LoadingPoint:loadingpoint,
      UnloadingPoint:UnLoading_point
    }
   )
  }

  allLoggedVeh()
  {
    console.log(this.logVehicle)
   return this.logVehicle
  }
}
