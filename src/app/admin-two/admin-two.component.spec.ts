import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTwoComponent } from './admin-two.component';

describe('AdminTwoComponent', () => {
  let component: AdminTwoComponent;
  let fixture: ComponentFixture<AdminTwoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminTwoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
