import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoggedVehiclesService } from '../logged-vehicles.service';

@Component({
  selector: 'app-data-book',
  templateUrl: './data-book.component.html',
  styleUrls: ['./data-book.component.css']
})
export class DataBookComponent implements OnInit {
@ViewChild("inputs") log:NgForm
  constructor(private logVehSer:LoggedVehiclesService) { }

  ngOnInit(): void {
  }

addLog()
{
console.log(this.log)
this.logVehSer.loggVeh(
  this.log.value.LoadingDate,
  this.log.value.VehicleNumber,
  this.log.value.Quantity,
  this.log.value.LoadingPoint,
  this.log.value.UnloadingPoint)
  alert(this.log.value.VehicleNumber+"  Logged successfully")
this.log.reset();
}
}
