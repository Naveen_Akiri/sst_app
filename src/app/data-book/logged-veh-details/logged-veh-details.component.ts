import { Component, OnInit } from '@angular/core';
import { logVehicle} from '../../Model';
import { LoggedVehiclesService } from '../../logged-vehicles.service';


@Component({
  selector: 'app-logged-veh-details',
  templateUrl: './logged-veh-details.component.html',
  styleUrls: ['./logged-veh-details.component.css']
})
export class LoggedVehDetailsComponent implements OnInit {

  constructor(private logvehser:LoggedVehiclesService) { }
  veh:logVehicle[]=[]
  ngOnInit(): void 
  {
    this.veh=this.logvehser.allLoggedVeh();
  }



}
