import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggedVehDetailsComponent } from './logged-veh-details.component';

describe('LoggedVehDetailsComponent', () => {
  let component: LoggedVehDetailsComponent;
  let fixture: ComponentFixture<LoggedVehDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoggedVehDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedVehDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
